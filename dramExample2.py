#dramExample.py
import dram as dram
import dampedOscillator as model
import numpy as np

class dram(dram.dram): #define a new Delayed Rejection Class with inheritance from the original model.

    def sumOfSquares(self, q):  #Define New sum of squares function
        ss = 0
        ydata = self.data['ydata']   #the xdata and ydata is saved as a dict and are referenced here
        xdata = self.data['xdata']   #I assume the data is in a numpy array
        size = xdata.size
        for i in range(xdata.size):
            ss += (model.model(q[0,0], q[0,1], q[0,2], xdata[i]) - ydata[i])**2
        # print ss
        return ss/size

    def delayedRejection(self, q, SSq, qstar, SSqstar, R, S2):  #turns off delayed rejection
        return q, SSq

def main():
    mcmc = dram()
    #note that my model is given by y = q1*x + q2*x*x
    xdata = np.linspace(0.0,10.0,1000)
    #Define fictional data with parameters q1 = 3, q2 = 1
    ydata = model.model(1.5,315,30,xdata)+ np.random.normal(0,.1,np.shape(xdata))
    mcmc.defineData(xdata, ydata) #pass data into the class
    mcmc.setChainLength(5000) #Define number of iterations
    mcmc.keepChain = True
    mcmc.initialGuess(np.array([[1.5,300,30]])) #makes initial Guess, 1, and 2
    mcmc.run() #Runs mcmc
    print("End First Run")
    # mcmc.plotChains()
    mcmc.run()
    print("End Second Run")
    print(mcmc.getMeanParameters()) #Print the mean values from the simulation
    mcmc.plotChains()
    mcmc.plotData()


if __name__ == '__main__':  #Run Script
    main()
