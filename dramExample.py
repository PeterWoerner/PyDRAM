#dramExample.py
import dram as dram
import modelExample1 as model
import numpy as np

class dram(dram.dram): #define a new Delayed Rejection Class with inheritance from the original model.

    def sumOfSquares(self, q):  #Define New sum of squares function
        ss = 0
        ydata = self.data['ydata']   #the xdata and ydata is saved as a dict and are referenced here
        xdata = self.data['xdata']   #I assume the data is in a numpy array
        size = xdata.size
        for i in range(xdata.size):
            ss += (model.model(xdata[i], q[0,0], q[0,1]) - ydata[i])**2
        # print ss
        return ss/size

def main():
    mcmc = dram()
    #note that my model is given by y = q1*x + q2*x*x
    xdata = np.linspace(0.0,10.0,100)
    #Define fictional data with parameters q1 = 3, q2 = 1
    ydata = xdata*3 + xdata*xdata*1 + np.random.normal(0,1,np.shape(xdata))
    mcmc.defineData(xdata, ydata) #pass data into the class
    mcmc.setChainLength(10000) #Define number of iterations
    mcmc.initialGuess(np.array([[1,2]])) #makes initial Guess, 1, and 2
    mcmc.run() #Runs mcmc
    print(mcmc.getMeanParameters()) #Print the mean values from the simulation
    mcmc.plotChains()
    # mcmc.plotS2Chain()
    # mcmc.plotData()


if __name__ == '__main__':  #Run Script
    main()
