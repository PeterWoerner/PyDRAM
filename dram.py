## Author: Peter Woerner
## June 30, 2017
#  Bug/Feature Add list
#   1.) Update/Include Recursive Formulas -- currently not working
#   2.) Add documentation for setting of parameters--gamma2, sp, k0, ns, numberofDelayedRejections
#   3.) Add post processing functions
#       a.)  Plot Parameter Correlations
#       b.)  Plot Posterior Densities
#       c.)  Calculate Mean Model Response and 95% Errors
#       d.)  Plot Mean Model Response and Errors
#   4.) Rework DelayedRejection function to make mathematically rigourous for more than 1 iteration
#   5.) Add a save data function for loading the data back into a python program
#       b.) Save data for a matlab format
#   6.) Add reject "burn in" for calculting variance matrix and mean
import numpy as np
from scipy.stats import invgamma
import scipy.linalg
import matplotlib.pyplot as plt

class dram():
    def __init__(self):
        #Set default initial values
        #We set the initial class to be a test case for a linear regression through some random data
        self.ns = 1
        self.M = 50
        self.gamma2 = 0.1
        xdata = np.linspace(0.0,100.0,1000)
        ydata = xdata*1 + np.random.normal(0,1,np.shape(xdata))
        self.data = {'xdata': xdata, 'ydata': ydata}
        self.q0 = np.array([[1e5]])
        self.k0 = 1
        self.S2Chain = []
        self.SSqChain = []
        self.parameterChain = []
        self.maxIter = 3
        self.keepChain = False
        self.autoS2 = True
        self.S2 = 1


    def defineData(self, xdata, ydata):
        #Please note that we assume xdata and ydata are 1-dimensional np arrays
        self.data['xdata'] = xdata
        self.data['ydata'] = ydata
        return 0

    def initialGuess(self, q):
        self.q0 = q.astype(float).reshape((1,np.size(q)))
        return 0

    def setChainLength(self, M):
        self.M = M
        return 0

    def run(self):
    # Step one set initial parameters (or read them in)
        sp = 2.3
        ns = self.ns #
        sigmas = 1 #standard deviation
        n = self.data['xdata'].size # Number of data points

        k0 = self.k0  #How frequently the covariance is updated

    # Step 2 get initial value
        #q0 = np.array([1, 2]]# Determine initial parameter set (to be read in)
        if len(self.parameterChain) == 0 or self.keepChain == False:
            q0 = self.q0
            q = q0
        else:
            q = self.parameterChain[-1]
            q0 = q
        p = len(q)

    # Step 3: Calculate initial sum of sumOfSquares
        if len(self.parameterChain) == 0 or self.keepChain == False:
            print("Starting New Chain")
            SSq = self.sumOfSquares(q)
        else: SSq = self.SSqChain[-1]

    # Step 4: Compute Initial Variance Estimate

        if self.autoS2 == True:
            S2 = SSq/abs(n-p)
            # print(self.autoS2, S2)
        else:
            S2 = self.S2
            # print(self.autoS2, S2)
        # print(S2)
    # Step 5: Consturct Covariance Estimate
        #V = S2*np.multiply(self.sens(q),np.transpose(self.sens(q))) Suggested, but we are going to start with the identity and no cross correlation


        #print V

        # print(len(self.parameterChain))
        if len(self.parameterChain) == 0 or self.keepChain == False:
            parameterChain = self.parameterChain
            parameterChain.append(q)
            S2Chain = self.S2Chain
            S2Chain.append(S2)
            SSqChain = self.SSqChain
            SSqChain.append(SSq)
        else:
            parameterChain = self.parameterChain
            S2Chain = self.S2Chain
            SSqChain = self.SSqChain
            # print(V)
            # print(SSqChain)
            # print(S2)
            # print(parameterChain)

        if len(self.parameterChain) == 0 or self.keepChain == False:
            V = sp*np.identity(q.size)
        else:
            # print(parameterChain[-1])
            V = sp*self._calcCovarianceMatrix(self.parameterChain)
            # print(V)
        I = np.identity(q.size)
        # print(np.linalg.eigvals(V))
        if not self._isPosDef(V):
            # print(self.isPosDef(V))
            epsilon = .01*np.amax(V)
            V = scipy.linalg.sqrtm(np.multiply(V,V.T) + epsilon*I)
        # if not self.isPosDef(V):
        #     V = scipy.linalg.sqrtm(np.multiply(V,V.T)) + epsilon*I
        R = np.linalg.cholesky(V)  #Cholesky decomposition of covariance matrix V
    # Step 6: Loop
        for k in range(self.M):
            # Step a
            zk = np.random.normal(0,1.0,np.shape(self.q0))

            #Step b
            # print(np.matmul(R,zk.T))
            # print(R, zk)
            # print('Test Shape', q0.shape,np.matmul(R,zk.T).T.shape, q0)
            qstar = (q0 + np.matmul(R,zk.T).T) #Next guess


            #Step c
            ualpha = np.random.rand(1)  #Random variable for accept reject of best guess

            #Step d
            SSqstar = self.sumOfSquares(qstar)  #Calculate sum of squares error (value to minimize)

            #Step e
            # print("S2 = " , S2)
            if S2 > 0:
                alpha = min(1, np.exp(-(SSqstar-SSq)/2/S2))
            else:
                alpha = min(1, np.exp(-(SSqstar-SSq)/2/1e-4))
                # print("Warning: S2 <= 0, setting S2 = 1e-4\n S2 = " + str(S2))
                # print("SSq = " + str(SSq))
            # print(alpha)
            #Step f
            # print(alpha, S2, 0.5*(ns*sigmas**2+SSq), SSq, SSqstar, q, qstar)
            # print(alpha, ualpha, SSqstar, SSq)
            if (ualpha < alpha):
                q = qstar
                SSq = SSqstar
            else:
                q, SSq = self.delayedRejection(q, SSq, qstar, SSqstar, R, S2)


            #Step g
            # print(self.autoS2)
            if self.autoS2 == True:
                S2 = invgamma.rvs(0.5*(ns+n), size=1, scale = 1.0/(0.5*(ns*sigmas**2+SSq)))
                # print(self.autoS2, S2)
            else:
                S2 = self.S2
                # print(self.autoS2, S2)


            S2Chain.append(S2)
            SSqChain.append(SSq)
            parameterChain.append(q)
            if k < 5:
                mean = np.mean(parameterChain)
                covariance  = self._calcCovarianceMatrix(parameterChain)
            else:
                mean, covariance = self._updateChainStats(mean, covariance, q, k)
            #Step h
            if k % self.k0 == 0:  ## Currently there is a bug in the updating of the covariance matrix
            #This is on the todo list to fix.
                # if k/k0 < 2 or k < 50:
                # print(k)
                V = sp*covariance + I*.01*np.amax(covariance)
                # else:
                    # V = sp*self.updateCovarianceMatrix(parameterChain, V/sp)
                #step i
                # print(np.linalg.eigvals(V))
                if not self._isPosDef(V):
                    # print(self.isPosDef(V))
                    epsilon = .01*np.amax(V)
                    V = scipy.linalg.sqrtm(np.multiply(V,V.T) + epsilon*I)
                    # print(self.isPosDef(V))
                if not self._isPosDef(V):
                    # print(self.isPosDef(V))
                    V = sp*np.identity(q.size)
                    # print(V)
                R = np.linalg.cholesky(V)


        #Post Processing
        # print(q)
        # fig, ax = plt.subplots(1, 1)
        # a = 4.07
        # x = np.linspace(0, 1000, 100000)
        # ax.plot(x, invgamma.pdf(0.5*(ns+n),0.5*(ns*sigmas*sigmas+x)),'r-', lw=5, alpha=0.6, label='invgamma pdf')
        # plt.show()
        self.S2Chain = S2Chain
        self.SSqChain = SSqChain
        self.parameterChain = parameterChain

        return S2Chain, SSqChain, parameterChain


    def _isPosDef(self, x):
        if np.any(np.isinf(x)) or np.any(np.isnan(x)):
            return 0
        else:
            return np.all(np.linalg.eigvals(x) > 0)

    def sumOfSquares(self, q): #Input parameter set (q) and data (d)  To be filled in
        ss = 0
        ydata = self.data['ydata']
        xdata = self.data['xdata']
        size = xdata.size
        for i in range(xdata.size):
            ss += (q*xdata[i]-ydata[i])*(q*xdata[i] - ydata[i])
        # print(ss)
        return ss/size

    def _updateChainStats(self, mean, covariance, theta, n):  #pass past mean, past covariance, new parameters, old number of iterations
        n = float(n)
        newMean = n/(n+1)*mean + 1/(n+1)*theta
        delta = theta - mean
        newCovariance = (n-1)/(n)*covariance + 1/(n+1)*np.dot(delta, delta.T)
        return newMean, newCovariance #updates mean, and covariance
        

    def _calcCovarianceMatrix(self, qlist):
        # print('Calculating Covariance Matrix')
        q0 = qlist[0]
        # print(qlist[-1], np.shape(q0))
        qshape = np.size(q0)
        qsum = 0*np.matmul(q0.T,q0).astype(float)
        # print('qsum0 = ', qsum)
        qkbar = 0*q0.astype(float)
        for q in qlist:
            # print(qsum.dtype, np.matmul(q,np.transpose(q)).dtype)

            qsum += np.matmul(np.transpose(q),q).astype(float)
            qkbar += q.astype(float)
        if len(qlist) > 1:
            return 1.0/(len(qlist)-1)*(qsum-np.matmul(np.transpose(qkbar),qkbar)/len(qlist))
        else: return np.identity(qshape)

    def delayedRejection(self, q, SSq, qstar, SSqstar, R, S2):
        i = 0
        maxIter = self.maxIter
        if maxIter < 1: maxIter = 1
        accept = False
        def J(q1, q2):
            Vinv = np.linalg.inv(R)*np.linalg.inv(np.conj(R))
            a = 1/np.sqrt((2*np.pi)**np.size(q)*np.linalg.norm(np.matmul(R,np.conj(R))))
            # print(np.matmul((q1-q2),Vinv))
            b = -0.5*np.matmul(np.matmul((q1-q2),Vinv),(q1-q2).T)
            # print(a)
            # print(b)
            return a*np.exp(b)

        def alpha(SSq1, SSq2):
            return min(1,np.exp(-0.5*(SSq1-SSq2)/S2))
        while i < maxIter and accept == False:
            i += 1
            #Step 1
            gamma2 = self.gamma2
            #Step 2
            zk = np.random.normal(0,1.0,np.shape(q))
            #Step 3
            qstar2 = q + gamma2*np.matmul(R,zk.T).T


            #Step 4
            ualpha = np.random.rand(1)

            #Step 5
            SSqstar2 = self.sumOfSquares(qstar2)
            #Step 6

            a = alpha(SSqstar2,SSq)
            J1 = J(qstar, qstar2)
            J2 = J(qstar, q)
            # print(q, qstar, qstar2, J1, J2)
            alpha2 = min(1.0, a*J1/J2*(1-alpha(SSqstar,SSqstar2)/(1-alpha(SSqstar,SSq))))

            if ualpha < alpha2:
                accept = True
                return qstar2,SSqstar2
            else:
                gamma2 = gamma2*self.gamma2
                # S2 = S2/10
        return q, SSq

    def plotS2Chain(self):
        q = np.vstack(self.S2Chain)
        n = len(self.parameterChain)
        x = np.arange(n)
        plt.scatter(x,q)
        plt.show()
        return 0

    def plotSSqChain(self):
        q = np.vstack(self.SSqChain)
        n = len(self.SSqChain)
        x = np.arange(n)
        plt.scatter(x,q)
        plt.show()
        return 0


    def plotChains(self):
        #plot chains #Currently works up to 4 parameters
        # print(self.parameterChain)
        q = np.vstack(self.parameterChain)
        # print(q)

        n = len(self.parameterChain)
        p = self.parameterChain[0].size #number of parameters
        x = np.arange(n)
        # print(p)
        # print(x)
        # print(q[:,0])
        # print(q[0,:])
        if p == 1:
            plt.scatter(x, q[:,0])
        elif p == 2:
            f, ((ax1), (ax2)) = plt.subplots(2, 1, sharex='col', sharey='row')
            ax1.scatter(x, q[:,0])
            ax2.scatter(x, q[:,1])
        elif p == 3:
            f, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, sharex='col', sharey='row')
            ax1.scatter(x, q[:,0])
            ax2.scatter(x, q[:,1])
            ax3.scatter(x, q[:,2])
        elif p == 4:
            f, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, sharex='col', sharey='row')
            ax1.scatter(x, q[:,0])
            ax2.scatter(x, q[:,1])
            ax3.scatter(x, q[:,2])
            ax4.scatter(x, q[:,3])
        elif p <= 9:
            f, ((ax1, ax2, ax3), (ax4, ax5, ax6), (ax7, ax8, ax9)) = plt.subplots(3, 3, sharex='col', sharey='row')
            ax1.scatter(x, q[:,0])
            ax2.scatter(x, q[:,1])
            ax3.scatter(x, q[:,2])
            ax4.scatter(x, q[:,3])
            ax5.scatter(x, q[:,4])
            if p > 5: ax6.scatter(x, q[:,5])
            if p > 6: ax7.scatter(x, q[:,6])
            if p > 7: ax8.scatter(x, q[:,7])
            if p > 8: ax9.scatter(x, q[:,8])

        plt.show()


        return 0


    def getMeanParameters(self):
        q = np.vstack(self.parameterChain)
        p = self.parameterChain[0].size
        qmean = []
        print("Getting Mean Parameters")
        print(p)
        for i in range(p):
            qmean.append(np.mean(q[:,i]))
            # print((i, np.mean(q[i,:])))

        return qmean

    def plotData(self):
        plt.scatter(self.data['xdata'], self.data['ydata'])
        # plt.plot(self.data['xdata'], self.data['xdata'])
        plt.show()

    def covarianceTest(self):
        np.random.seed(1)
        thetas = []
        thetas.append(np.random.randn(10,1))
        thetas.append(np.random.randn(10,1))
        thetas.append(np.random.randn(10,1))
        j = len(thetas)
        means = []
        npmeans = []
        a , b, c= np.asarray(thetas).shape
        mean = np.mean(thetas, axis = 0)
        covariance = np.cov(np.asarray(thetas).reshape(a,b), rowvar = False).T
        for i in range(10):
            thetas.append(np.random.randn(10,1))
            mean, covariance = self._updateChainStats(mean, covariance, thetas[-1], len(thetas)-1)
        a , b, c= np.asarray(thetas).shape
        cov = np.cov(np.asarray(thetas).reshape(a,b), rowvar = False)    
        assert(mean.all()==np.mean(thetas, axis =0).all())
        assert(cov.all() == covariance.all())
        return

def main():
    # Run Dram()
    mcmc = dram()
    mcmc.covarianceTest()
    # mcmc.plotData()
    #Covariance Check
    # q = []
    # for i in range(1000000):
    #     q.append(np.random.normal(0.0,1.0,(10,1)))
    #     # print(q[i])
    # #q = [np.reshape(np.array([0,2]),(2,1)), np.reshape(np.array([1,1]),(2,1)), np.reshape(np.array([2,0]),(2,1))]
    #
    # V = mcmc.calcCovarianceMatrix(q)
    # print(V)
    #Dram Check
    mcmc.setChainLength(1000)
    mcmc.initialGuess(np.array([[1.0]]))
    mcmc.run()
    print(mcmc.getMeanParameters())
    # mcmc.plotChains()

if __name__ == '__main__':
    main()
