#modelExample1
import numpy as np
def model(y0,wn,C, t):
    y = y0*np.exp(-C*t/2)*np.cos((wn**2-C**2/4)**0.5*t);
    # print(y)
    return y
